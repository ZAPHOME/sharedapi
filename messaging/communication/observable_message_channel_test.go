package communication

import (
	"testing"
	"bitbucket.org/zaphome/sharedapi/messaging"
	"time"
	"reflect"
)

func TestAddObserver(t *testing.T) {
	// GIVEN: Two different messages
	messageA := messaging.AbstractInstruction{}
	messageB := messaging.AbstractEvent{}

	// and: Three different observers
	observerA := Observer(func(message messaging.Message) {})
	observerB := Observer(func(message messaging.Message) {})
	observerC := Observer(func(message messaging.Message) {})

	// WHEN: Creating a message bus with one observer for message A
	// and two observers for message B
	mc := GetNewObservableMessageChannelFromChannel(make(chan messaging.Message))
	mc.AddObserver(reflect.TypeOf(messageA), &observerA)
	mc.AddObserver(reflect.TypeOf(messageB), &observerB)
	mc.AddObserver(reflect.TypeOf(messageB), &observerC)

	// THEN: The message bus has registered one observer for message A
	if len(mc.Observers[reflect.TypeOf(messageA)]) != 1 {
		t.Error("One observer on message A expected")
		t.Fail()
	}

	// and: two observers for message B
	if len(mc.Observers[reflect.TypeOf(messageB)]) != 2 {
		t.Error("Two observer on message B expected")
		t.Fail()
	}
}

func TestRemoveObserver(t *testing.T) {
	// GIVEN: Two different messages
	messageA := messaging.AbstractInstruction{}
	messageB := messaging.AbstractEvent{}

	// and: Three different observers
	observerA := Observer(func(message messaging.Message) {})
	observerB := Observer(func(message messaging.Message) {})
	observerC := Observer(func(message messaging.Message) {})

	// WHEN: Creating a message bus with one observer for message A
	// and two observers for message B
	mc := GetNewObservableMessageChannelFromChannel(make(chan messaging.Message))
	mc.AddObserver(reflect.TypeOf(messageA), &observerA)
	mc.AddObserver(reflect.TypeOf(messageB), &observerB)
	mc.AddObserver(reflect.TypeOf(messageB), &observerC)

	// and: Removing observer 1 and 2
	mc.RemoveObserver(reflect.TypeOf(messageA), &observerA)
	mc.RemoveObserver(reflect.TypeOf(messageB), &observerB)

	// THEN: There is no observer on message A
	if len(mc.Observers[reflect.TypeOf(messageA)]) != 0 {
		t.Error("No observer on message A expected")
		t.Fail()
	}

	// and: One observer on message B
	if len(mc.Observers[reflect.TypeOf(messageB)]) != 1 {
		t.Error("One observer on message B expected")
		t.Fail()
	}
}

func TestAddGlobalObserver(t *testing.T) {
	// GIVEN: Two observers
	observerA := Observer(func(message messaging.Message) {})
	observerB := Observer(func(message messaging.Message) {})

	// WHEN: Creating a message bus with two global observers
	mc := GetNewObservableMessageChannelFromChannel(make(chan messaging.Message))
	mc.AddGlobalObserver(&observerA)
	mc.AddGlobalObserver(&observerB)

	// THEN: The message bus has registered two global observer
	if len(mc.GlobalObservers) != 2 {
		t.Error("Two global observers expected")
		t.Fail()
	}
}

func TestRemoveGlobalObserver(t *testing.T) {
	// GIVEN: Two observers
	observerA := Observer(func(message messaging.Message) {})
	observerB := Observer(func(message messaging.Message) {})

	// WHEN: Creating a message bus with two global observers
	mc := GetNewObservableMessageChannelFromChannel(make(chan messaging.Message))
	mc.AddGlobalObserver(&observerA)
	mc.AddGlobalObserver(&observerB)

	// and: Removing observer 1
	mc.RemoveGlobalObserver(&observerA)

	// THEN: The message bus has registered one global observer
	if len(mc.GlobalObservers) != 1 {
		t.Error("One global observer expected")
		t.Fail()
	}
}

func TestListening(t *testing.T) {
	// GIVEN: Two different messages
	messageA := messaging.AbstractInstruction{}
	messageB := messaging.AbstractEvent{}

	// and: A counter for three different observers
	observerACalls := 0
	observerBCalls := 0
	observerCCalls := 0
	observerDCalls := 0

	// and: Four different observers
	observerA := Observer(func(message messaging.Message) { observerACalls++ })
	observerB := Observer(func(message messaging.Message) { observerBCalls++ })
	observerC := Observer(func(message messaging.Message) { observerCCalls++ })
	observerD := Observer(func(message messaging.Message) { observerDCalls++ })

	// WHEN: Creating a message bus with one observer for message A, two observers for message B
	// and: one global observer
	mc := GetNewObservableMessageChannelFromChannel(make(chan messaging.Message))
	mc.AddObserver(reflect.TypeOf(messageA), &observerA)
	mc.AddObserver(reflect.TypeOf(messageB), &observerB)
	mc.AddObserver(reflect.TypeOf(messageB), &observerC)
	mc.AddGlobalObserver(&observerD)

	// and: Start the observing
	mc.StartObserving()

	// WHEN: Sending a message A
	mc.Channel <- messageA
	time.Sleep(1000000)

	// THEN: One call is on observers A and D
	if observerACalls != 1 || observerBCalls != 0 || observerCCalls != 0 || observerDCalls != 1 {
		t.Error("One call on observers A and D expected")
		t.Fail()
	}

	// WHEN: Sending a message A
	mc.Channel <- messageB
	time.Sleep(1000000)

	// THEN: One new call is on observers B, C and D
	// and: No new calls on observer A
	if observerACalls != 1 || observerBCalls != 1 || observerCCalls != 1 || observerDCalls != 2 {
		t.Error("One call on each observer and two calls on the global observer expected")
		t.Fail()
	}

	// WHEN: Stop the observing
	mc.StopObserving()
	// and: Sending a message B
	mc.Channel <- messageB
	time.Sleep(1000000)

	// THEN: No new calls on each observer
	if observerACalls != 1 || observerBCalls != 1 || observerCCalls != 1 || observerDCalls != 2 {
		t.Error("One call on each observer and two calls on the global observer expected")
		t.Fail()
	}
}
