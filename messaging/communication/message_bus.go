package communication

import (
	"bitbucket.org/zaphome/sharedapi/messaging"
	"time"
)

type MessageBus struct {
	SourceChannel      *ObservableMessageChannel
	DestinationChannel *ObservableMessageChannel
}

func GetNewMessageBus() *MessageBus {
	return &MessageBus{
		SourceChannel:      GetNewObservableMessageChannelFromChannel(make(chan messaging.Message)),
		DestinationChannel: GetNewObservableMessageChannelFromChannel(make(chan messaging.Message)),
	}
}

func GetNewMessageBusWithSource(channel *ObservableMessageChannel) *MessageBus {
	return &MessageBus{
		SourceChannel:      channel,
		DestinationChannel: GetNewObservableMessageChannelFromChannel(make(chan messaging.Message)),
	}
}

func GetNewMessageBusWithDestination(channel *ObservableMessageChannel) *MessageBus {
	return &MessageBus{
		SourceChannel:      GetNewObservableMessageChannelFromChannel(make(chan messaging.Message)),
		DestinationChannel: channel,
	}
}

func (mb *MessageBus) StartObserving()  {
	mb.SourceChannel.StartObserving()
	mb.DestinationChannel.StartObserving()
}

func (mb *MessageBus) StopObserving()  {
	mb.SourceChannel.StopObserving()
	mb.DestinationChannel.StopObserving()
}

type TimeoutError struct {
	Message string
}

func (e TimeoutError) Error() string {
	return e.Message
}

func (mb *MessageBus) SendRequestToDestinationAndWaitForResponse(request messaging.Request) (response *messaging.Response, err error) {
	return sendRequestAndWaitForResponse(request, mb.SourceChannel, mb.DestinationChannel)
}

func (mb *MessageBus) SendRequestToSourceAndWaitForResponse(request messaging.Request) (response *messaging.Response, err error) {
	return sendRequestAndWaitForResponse(request, mb.DestinationChannel, mb.SourceChannel)
}

func sendRequestAndWaitForResponse(request messaging.Request, inputChannel *ObservableMessageChannel,
		outputChannel *ObservableMessageChannel) (response *messaging.Response, err error) {
	// Create a new channel for the response
	resultChannel := make(chan messaging.Response, 1)

	// Define a global observer that listens for the response
	// When the response was found, put it on the resultChannel
	observer := Observer(func(res messaging.Message) {
		r, ok := res.(messaging.Response)
		if ok {
			if r.GetId() == request.GetId() {
				resultChannel <- r
			}
		}
	})
	inputChannel.AddGlobalObserver(&observer)
	defer inputChannel.RemoveGlobalObserver(&observer)

	// Send the request
	outputChannel.SendMessage(request)

	// Wait maximal 5 seconds for the response
	select {
	case result := <- resultChannel:
		response = &result
	case <- time.After(time.Second * 5):
		err = TimeoutError{"No answer. 5s timeout reached"}
	}

	// Return the result
	return
}
