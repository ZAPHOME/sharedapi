package communication

import (
	"testing"
	"bitbucket.org/zaphome/sharedapi/messaging"
	"fmt"
	"reflect"
)

type dummyRequest struct {
	messaging.Request
}

type dummyResponse struct {
	messaging.Response
}

func buildDummyRequest(destination int) *dummyRequest {
	request := messaging.BuildRequest(0, destination)
	return &dummyRequest{request }
}

func buildDummyResponse(source int, id int) *dummyResponse {
	response := messaging.BuildResponse(source, id)
	return &dummyResponse{response }
}

func TestSendRequestAndWaitForResponse(t *testing.T) {
	// GIVEN: A message bus
	bus := GetNewMessageBus()
	bus.StartObserving()

	// and: an observer for the destination channel that sends an dummy Response
	observerDestination := Observer(func(message messaging.Message) {
		request := message.(*dummyRequest)
		response := buildDummyResponse(0, request.GetId())
		fmt.Printf("Received request: %+v. Sending response: %+v", request, response)
		bus.SourceChannel.SendMessage(response)
	})
	bus.DestinationChannel.AddObserver(reflect.TypeOf(&dummyRequest{}), &observerDestination)

	// WHEN: Sending a dummy Request
	request := buildDummyRequest(0)
	response, err := bus.SendRequestToDestinationAndWaitForResponse(request)

	// THEN: The dummy Response is received
	if err != nil {
		t.Error("An error appears while sending a message: " + err.Error())
		t.Fail()
	}

	// and: the response is valid
	if response == nil {
		t.Errorf("No response was received")
		t.Fail()
	}
	hcResponse, ok := (*response).(*dummyResponse)
	if ok {
		if hcResponse.GetId() != request.GetId() {
			t.Errorf("Received a wrong response: %+v", hcResponse)
			t.Fail()
		}
	} else {
		t.Error("The response has the wrong type!")
		t.Fail()
	}
}

func TestTimeoutError(t *testing.T) {
	// GIVEN: A message bus
	bus := GetNewMessageBus()
	bus.StartObserving()

	// WHEN: Sending a dummy Request
	request := buildDummyRequest(0)
	_, err := bus.SendRequestToDestinationAndWaitForResponse(request)

	// THEN: A timeout was reached, because there is no response sent.
	if err == nil {
		t.Error("An timeout was expected.")
		t.Fail()
	}
}
