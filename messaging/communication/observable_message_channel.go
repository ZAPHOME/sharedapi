package communication

import (
	"bitbucket.org/zaphome/sharedapi/messaging"
	"sync"
	"reflect"
)

type Observer func(message messaging.Message)

type ObservableMessageChannel struct {
	Channel         chan messaging.Message
	listening       bool
	listeningMutex  sync.Mutex
	Observers       map[reflect.Type][]*Observer
	GlobalObservers []*Observer
}

func GetNewObservableMessageChannelFromChannel(channel chan messaging.Message) *ObservableMessageChannel {
	return &ObservableMessageChannel{
		Channel:         channel,
		listening:       false,
		listeningMutex:  sync.Mutex{},
		Observers:       make(map[reflect.Type][]*Observer),
		GlobalObservers: []*Observer{},
	}
}

func (mc *ObservableMessageChannel) AddObserverFunction(messageType reflect.Type,
		observerFunc func(message messaging.Message)) *Observer {
	o := Observer(observerFunc)
	mc.AddObserver(messageType, &o)
	return &o
}

func (mc *ObservableMessageChannel) AddObserver(messageType reflect.Type, observer *Observer) {
	if mc.Observers[messageType] == nil {
		mc.Observers[messageType] = []*Observer{observer}
	} else {
		mc.Observers[messageType] = append(mc.Observers[messageType], observer)
	}
}

func (mc *ObservableMessageChannel) RemoveObserver(messageType reflect.Type, observer *Observer) {
	newObservers := []*Observer{}
	for _, oldObserver := range mc.Observers[messageType] {
		if oldObserver != observer {
			newObservers = append(newObservers, oldObserver)
		}
	}
	mc.Observers[messageType] = newObservers
}

func (mc *ObservableMessageChannel) AddGlobalObserverFunction(
		observerFunc func(message messaging.Message)) *Observer {
	o := Observer(observerFunc)
	mc.AddGlobalObserver(&o)
	return &o
}

func (mc *ObservableMessageChannel) AddGlobalObserver(observer *Observer) {
	if mc.GlobalObservers == nil {
		mc.GlobalObservers = []*Observer{observer}
	} else {
		mc.GlobalObservers = append(mc.GlobalObservers, observer)
	}
}

func (mc *ObservableMessageChannel) RemoveGlobalObserver(observer *Observer) {
	newObservers := []*Observer{}
	for _, oldObserver := range mc.GlobalObservers {
		if oldObserver != observer {
			newObservers = append(newObservers, oldObserver)
		}
	}
	mc.GlobalObservers = newObservers
}

func (mc *ObservableMessageChannel) SendMessage(message messaging.Message) *messaging.Message{
	// TODO create and call send interceptors
	if mc.listening {
		mc.Channel <- message
		return &message
	} else {
		return nil
	}
}

func (mc *ObservableMessageChannel) IsObserving() bool {
	return mc.listening
}

func (mc *ObservableMessageChannel) StartObserving() {
	mc.listeningMutex.Lock()
	defer mc.listeningMutex.Unlock()

	mc.listening = true
	go listenOnObservableMessageChannel(mc)
}

func (mc *ObservableMessageChannel) StopObserving() {
	mc.listeningMutex.Lock()
	defer mc.listeningMutex.Unlock()

	mc.listening = false
}

func listenOnObservableMessageChannel(omc *ObservableMessageChannel) {
	for omc.listening {
		if message := <- omc.Channel; omc.listening {
			omc.processMessage(message)
		} else {
			break
		}
	}
}

func (mc *ObservableMessageChannel) processMessage(message messaging.Message) {
	// TODO create and call receive interceptors
	for _, observer := range mc.Observers[reflect.TypeOf(message)] {
		observerFunction := *observer
		go observerFunction(message)
	}
	for _, observer := range mc.GlobalObservers {
		observerFunction := *observer
		go observerFunction(message)
	}
}
