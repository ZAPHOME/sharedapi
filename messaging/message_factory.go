package messaging

import "sync"

var (
	requestId      int
	requestIdMutex sync.Mutex
)

func BuildInstruction(source int, destination int) AbstractInstruction {
	return AbstractInstruction{
		SourceId:      source,
		DestinationId: destination,
	}
}

func BuildEvent(source int) AbstractEvent {
	return AbstractEvent{
		SourceId: source,
	}
}

func BuildRequest(source int, destination int) Request {
	requestIdMutex.Lock()
	requestId++
	defer requestIdMutex.Unlock()

	return AbstractRequest{
		Instruction: BuildInstruction(source, destination),
		Id: requestId,
	}
}

func BuildResponse(source int, id int) Response {
	return AbstractResponse{
		Event: BuildEvent(source),
		Id: id,
	}
}
