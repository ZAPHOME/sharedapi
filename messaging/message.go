package messaging

// Message
type Message interface{
	GetSourceId() int
}


// Instruction
type Instruction interface {
	Message
	GetDestinationId() int
}

type AbstractInstruction struct {
	SourceId      int
	DestinationId int
}

func (a AbstractInstruction) GetSourceId() int {
	return a.SourceId
}

func (a AbstractInstruction) GetDestinationId() int {
	return a.DestinationId
}


// Event
type Event interface {
	Message
}

type AbstractEvent struct {
	SourceId int
}

func (a AbstractEvent) GetSourceId() int {
	return a.SourceId
}


// Request
type Request interface {
	Instruction
	GetId() int
}

type AbstractRequest struct {
	Instruction AbstractInstruction
	Id          int
}

func (a AbstractRequest) GetSourceId() int {
	return a.Instruction.SourceId
}

func (a AbstractRequest) GetDestinationId() int {
	return a.Instruction.DestinationId
}

func (r AbstractRequest) GetId() int {
	return r.Id
}

// Response
type Response interface {
	Event
	GetId() int
}

type AbstractResponse struct {
	Event AbstractEvent
	Id    int
}

func (a AbstractResponse) GetSourceId() int {
	return a.Event.SourceId
}

func (r AbstractResponse) GetId() int {
	return r.Id
}
