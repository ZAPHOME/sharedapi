package logging

import (
	"os"
	"io"
	"fmt"
)

var (
	DEFAULT_LOG_LEVEL = DEBUG_LOG_LEVEL // TODO change to info
	DEFAULT_DATE_FORMAT = "2006-01-02 15:04:05" // yyyy-MM-dd hh:mm:ss
	DEFAULT_FORMAT = fmt.Sprintf("%s[%s | %s | %s] %s%s", // [time | level | name] message
		LIGHT_BLUE_FOREGROUND, DATE_TIME_LOG_FORMAT,
		LOG_LEVEL_LOG_FORMAT, LOGGER_NAME_LOG_FORMAT,
		DEFAULT_FOREGROUND, MESSAGE_LOG_FORMAT)
)

func CreateConsoleLogger(name string) *Logger {
	return CreateLogger(name, os.Stdout)
}

func CreateFileLogger(name, file_path string) (*Logger, error) {
	file, err := os.OpenFile(file_path, os.O_APPEND, 0600)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return CreateLogger(name, file), nil
}

func CreateLogger(name string, writer io.Writer) *Logger {
	return &Logger{ name, DEFAULT_FORMAT, *DEFAULT_LOG_LEVEL, &writer }
}
