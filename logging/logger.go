package logging

import (
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

type LogLevel struct {
	Id   int
	Name string
}

var (
	DEBUG_LOG_LEVEL = &LogLevel{0, "Debug"}
	INFO_LOG_LEVEL  = &LogLevel{1, "Info"}
	WARN_LOG_LEVEL  = &LogLevel{2, "Warn"}
	ERROR_LOG_LEVEL = &LogLevel{3, "Error"}
	PANIC_LOG_LEVEL = &LogLevel{4, "Panic"}
	FATAL_LOG_LEVEL = &LogLevel{5, "Fatal"}
)

const (
	DATE_TIME_LOG_FORMAT   = "{date_time}"
	LOG_LEVEL_LOG_FORMAT   = "{log_level}"
	LOGGER_NAME_LOG_FORMAT = "{logger_name}"
	MESSAGE_LOG_FORMAT     = "{message}"
)

type Logger struct {
	Name   string
	Format string
	Level  LogLevel
	Writer *io.Writer
}

func (logger *Logger) Debug(format string, objects ...interface{}) (int, error) {
	message := logger.formatLogEntry(DEBUG_LOG_LEVEL, format, objects)
	return logger.writeMessageToLogger(message, DEBUG_LOG_LEVEL)
}

func (logger *Logger) Info(format string, objects ...interface{}) (int, error) {
	message := logger.formatLogEntry(INFO_LOG_LEVEL, format, objects)
	return logger.writeMessageToLogger(message, INFO_LOG_LEVEL)
}

func (logger *Logger) InfoOnError(e error) (n int, err error) {
	if e == nil {
		return
	}
	return logger.Info(e.Error())
}

func (logger *Logger) Warn(format string, objects ...interface{}) (int, error) {
	message := logger.formatLogEntry(WARN_LOG_LEVEL, format, objects)
	return logger.writeMessageToLogger(message, WARN_LOG_LEVEL)
}

func (logger *Logger) WarnOnError(e error) (n int, err error) {
	if e == nil {
		return
	}
	return logger.Warn(e.Error())
}

func (logger *Logger) Error(format string, objects ...interface{}) (int, error) {
	message := logger.formatLogEntry(ERROR_LOG_LEVEL, format, objects)
	return logger.writeMessageToLogger(message, ERROR_LOG_LEVEL)
}

func (logger *Logger) ErrorOnError(e error) (n int, err error) {
	if e == nil {
		return
	}
	return logger.Error(e.Error())
}

func (logger *Logger) Panic(format string, objects ...interface{}) (n int, err error) {
	message := logger.formatLogEntry(PANIC_LOG_LEVEL, format, objects)
	n, err = logger.writeMessageToLogger(message, PANIC_LOG_LEVEL)
	panic(message)
	return
}

func (logger *Logger) PanicOnError(e error) (n int, err error) {
	if e == nil {
		return
	}
	return logger.Panic(e.Error())
}

func (logger *Logger) Fatal(format string, objects ...interface{}) (n int, err error) {
	message := logger.formatLogEntry(FATAL_LOG_LEVEL, format, objects)
	n, err = logger.writeMessageToLogger(message, FATAL_LOG_LEVEL)
	os.Exit(-1)
	return
}

func (logger *Logger) FatalOnError(e error) (n int, err error) {
	if e == nil {
		return
	}
	return logger.Fatal(e.Error())
}

func (logger *Logger) formatLogEntry(log_level *LogLevel, format string, objects []interface{}) (result string) {
	// The message has to be the first entry.
	// So, the message could also contain "{date_time}" to print the date and the time
	result = strings.Replace(logger.Format, MESSAGE_LOG_FORMAT, fmt.Sprintf(format, objects...), -1)
	result = strings.Replace(result, DATE_TIME_LOG_FORMAT, time.Now().Format(DEFAULT_DATE_FORMAT), -1)
	result = strings.Replace(result, LOG_LEVEL_LOG_FORMAT, fmt.Sprintf("%-5s", log_level.Name), -1)
	result = strings.Replace(result, LOGGER_NAME_LOG_FORMAT, logger.Name, -1)
	// new log formats: result = strings.Replace(result, <placeholder>, <value>, -1)
	if !strings.HasSuffix(result, "\n") {
		result += "\n"
	}
	return
}

func (logger *Logger) writeMessageToLogger(message string, log_level *LogLevel) (int, error) {
	if log_level.Id >= logger.Level.Id {
		return (*logger.Writer).Write([]byte(message))
	} else {
		return 0, nil
	}
}
