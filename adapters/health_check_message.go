package adapters

import "bitbucket.org/zaphome/sharedapi/messaging"

type AdapterHealthCheckRequest struct {
	messaging.Request
}

type AdapterHealthCheckResponse struct {
	messaging.Response
}

func BuildAdapterHealthCheckRequest(source int, destination int) *AdapterHealthCheckRequest {
	request := messaging.BuildRequest(source, destination)
	return &AdapterHealthCheckRequest{ request }
}

func BuildAdapterHealthCheckResponse(source int, id int) *AdapterHealthCheckResponse {
	response := messaging.BuildResponse(source, id)
	return &AdapterHealthCheckResponse{ response }
}
