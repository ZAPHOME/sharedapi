package actuators

import "bitbucket.org/zaphome/sharedapi/adapters/actuators/states"

type Actuator struct {
	Id        int
	AdapterId int
	Name      string
	State     states.State
}
