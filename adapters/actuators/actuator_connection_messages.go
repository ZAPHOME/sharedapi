package actuators

import "bitbucket.org/zaphome/sharedapi/messaging"

type ActuatorConnectedEvent struct {
	messaging.Event
	Actuator
}

func BuildActuatorConnectedEvent(source int, actuator Actuator) *ActuatorConnectedEvent {
	request := messaging.BuildEvent(source)
	return &ActuatorConnectedEvent{ request, actuator }
}
