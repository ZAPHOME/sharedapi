package states

import "bitbucket.org/zaphome/sharedapi/messaging"

type ActuatorStateChangeInstruction struct {
	messaging.Instruction
	ActuatorId            int
	State                 State
}

type ActuatorStateChangedEvent struct {
	messaging.Event
	ActuatorId      int
	State           State
}

// TODO Set destination to CENTRAL_CHANNEL for standard
func BuildActuatorStateChangeInstruction(source int, destination int, actuatorId int, state State) *ActuatorStateChangeInstruction {
	instruction := messaging.BuildInstruction(source, destination)
	return &ActuatorStateChangeInstruction{ instruction, actuatorId, state }
}

func BuildActuatorStateChangedEvent(source int, actuatorId int, state State) *ActuatorStateChangedEvent {
	event := messaging.BuildEvent(source)
	return &ActuatorStateChangedEvent{ event, actuatorId, state }
}
