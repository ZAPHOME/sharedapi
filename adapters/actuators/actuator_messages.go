package actuators

import "bitbucket.org/zaphome/sharedapi/messaging"

type ActuatorListRequest struct {
	messaging.Request
}

type ActuatorListResponse struct {
	messaging.Response
	ActuatorList []Actuator
}

// TODO Set destination to CENTRAL_CHANNEL for standard
func BuildActuatorListRequest(source int, destination int) *ActuatorListRequest {
	request := messaging.BuildRequest(source, destination)
	return &ActuatorListRequest{ request }
}

func BuildActuatorListResponse(source int, id int, actuatorList []Actuator) *ActuatorListResponse {
	response := messaging.BuildResponse(source, id)
	return &ActuatorListResponse{ response, actuatorList }
}
