package adapters

import "bitbucket.org/zaphome/sharedapi/messaging"

type AdapterListRequest struct {
	messaging.Request
}

type AdapterListResponse struct {
	messaging.Response
	AdapterList []Adapter
}

// TODO Set destination to CENTRAL_CHANNEL for standard
func BuildAdapterListRequest(source int, destination int) *AdapterListRequest {
	request := messaging.BuildRequest(source, destination)
	return &AdapterListRequest{ request }
}

func BuildAdapterListResponse(source int, id int, adapterList []Adapter) *AdapterListResponse {
	response := messaging.BuildResponse(source, id)
	return &AdapterListResponse{ response, adapterList }
}
