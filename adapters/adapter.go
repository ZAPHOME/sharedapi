package adapters

import "bitbucket.org/zaphome/sharedapi/messaging/communication"

type Adapter struct {
	Id         int
	Name       string
	MessageBus *communication.MessageBus
}
