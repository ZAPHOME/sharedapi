package context

import (
	"reflect"
	"sync"
)

type Context struct {
	Id int
	Services map[string]*interface{}
}

var nextIdMutex = sync.Mutex{}
var nextId = 0

func CreateEmptyContext() *Context {
	return &Context{
		Id: getNextId(),
		Services: make(map[string]*interface{}, 0),
	}
}

func getNextId() (nextId int) {
	nextIdMutex.Lock()
	defer nextIdMutex.Unlock()
	nextId++
	return
}

func (context *Context) AddServiceWithTypeName(service interface{}) (*interface{}, error) {
	name := reflect.TypeOf(service).Name()
	return context.AddService(name, service)
}

type ServiceAlreadyExistsError struct {
	ErrorMessage string
}

func (e ServiceAlreadyExistsError) Error() string {
	return e.ErrorMessage
}

func (context *Context) AddService(name string, service interface{}) (*interface{}, error) {
	if context.Services[name] == nil {
		servicePtr := &service
		context.Services[name] = servicePtr
		return servicePtr, nil
	} else {
		return nil, ServiceAlreadyExistsError{
			"A service with the name '" + name + "' already exists in this context",
		}
	}
}

func (context *Context) RemoveService(name string) {
	context.Services[name] = nil
}

func (context *Context) GetService(name string) *interface{} {
	return context.Services[name]
}
