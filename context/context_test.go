package context

import "testing"

func TestAddService(t *testing.T) {
	// GIVEN: An empty context
	context := CreateEmptyContext()
	type TestStruct struct {
		SomeString string
	}

	// WHEN: Adding three services
	context.AddService("test1", TestStruct{"adshrehj"})
	context.AddService("test2", TestStruct{"Teest"})
	context.AddService("test3", TestStruct{"45j45eqd"})

	// THEN: The service test2 contains the value 'Teest'
	if service, ok := (*context.GetService("test2")).(TestStruct); ok {
		if service.SomeString != "Teest" {
			t.Fatal("The service has not the expected values")
		}
		// If SomeString is "Teest" the test passed!
	} else {
		t.Fatal("The service is not a 'TestStruct', but this is expected")
	}
}

func TestRemoveService(t *testing.T) {
	// GIVEN: An empty context
	context := CreateEmptyContext()
	type TestStruct struct {
		SomeString string
	}

	// WHEN: Add 3 service
	context.AddService("test1", TestStruct{"adshrehj"})
	context.AddService("test2", TestStruct{"Teest"})
	context.AddService("test3", TestStruct{"45j45eqd"})
	// AND: Remove the second
	context.RemoveService("test2")

	// THEN: The context contains two services
	if context.Services["test1"] == nil {
		t.Fatal("The first service could not be found")
	}

	if context.Services["test2"] != nil {
		t.Fatal("The secound service could be found, but was deleted")
	}

	if context.Services["test3"] == nil {
		t.Fatal("The third service could not be found")
	}
}

